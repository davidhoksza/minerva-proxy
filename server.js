const   http = require('http'),
        https = require('https'),
        url = require('url'),
        fs = require('fs');


let config = require('./config');

if (fs.existsSync('local_config.js')) {

  function isObject(item) {
    return (item && typeof item === 'object' && !Array.isArray(item));
  }


  function mergeDeep(target, ...sources) {
    if (!sources.length) return target;
    const source = sources.shift();

    if (isObject(target) && isObject(source)) {
      for (const key in source) {
        if (source.hasOwnProperty(key)) {
          if (isObject(source[key])) {
            if (!target[key]) Object.assign(target, {[key]: {}});
            mergeDeep(target[key], source[key]);
          } else {
            Object.assign(target, {[key]: source[key]});
          }
        }
      }
    }

    return mergeDeep(target, ...sources);
  }

  //add local setting to default settings
  let localConfig = require('./local_config.js');
  mergeDeep(config, localConfig);
}


const ServerError = function(message, extra) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message;
    this.extra = extra;
};

const minervaTokens = {};

const serverCallback = function (req, res) {
    try {
        if (req.url === '/favicon.ico') {
            res.writeHead(200, {'Content-Type': 'image/x-icon'});
            res.end();
            return;
        }

        const options = parseRequest(req);
        options.headers = req.headers;
        delete options.headers.host; //otherwise the host header would be the proxy address
        options.method = req.method;
        options.agent = false;

        const apiEndPoint = getAPIEndPoint(options.href);
        if (apiEndPoint in minervaTokens) {
            passRequest(req, res, options, minervaTokens[apiEndPoint]);
        } else {
            const handler = options.protocol === 'https:' ? https : http;
            const urlObject = url.parse(getLoginEndPoint(apiEndPoint));
            urlObject.rejectUnauthorized = false;
            const hg = handler.get(urlObject, function (response) {
                minervaTokens[apiEndPoint] = getMinervaAuthTokenFromHeader(response);
                passRequest(req, res, options, minervaTokens[apiEndPoint])
            });
            hg.on('error', function (err) {
                res.writeHead(404, {'Content-Type': 'text/plain'});
                res.write('The target server is not reachable.');
                res.end();
            });
        }
    } catch (err) {
        if (err instanceof ServerError) {
            res.writeHead(403, {'Content-Type': 'text/plain'});
            res.write(err.message);
            res.end();
        } else throw err;
    }
};

server = config.useHttps ? https.createServer(config.httpsOptions,serverCallback) : http.createServer(serverCallback);
server.listen(config.portToListen);

function getMinervaAuthTokenFromHeader(res){
    const setCookie = res.headers['set-cookie'];
    return /MINERVA_AUTH_TOKEN=([A-Z0-9]+)/.exec(setCookie)[1];
}

function getLoginEndPoint(apiEndPoint) {
    return apiEndPoint + 'doLogin';
}

function passRequest(req, res, options, minervaAuthToken) {
    options.headers.cookie += '; MINERVA_AUTH_TOKEN=' + minervaAuthToken;
    const handler = options.protocol === 'https:' ? https : http;
    options.rejectUnauthorized = false;
    const connector = handler.request(options, function(response) {
        response.pipe(res, {end:true});//tell 'response' end=true
    });
    req.pipe(connector, {end:true});
}

function isAllowedServer(href) {
    for (let instance of config.allowedInstances) {
        if (href.indexOf(instance) >= 0) {
            return true;
        }
    }
    return false;
}

function getAPIEndPoint(href) {
    const apiEndPoint = /.*\/api\//.exec(href);
    if (!apiEndPoint)
        throw new ServerError('API endpoint not found in the target address.');

    return apiEndPoint[0]
}

function parseRequest(req) {
    const urlObj = url.parse(req.url, true);
    const query = urlObj.query;
    if (!('url' in query))
        throw new ServerError('Missing \'url\' in query string.');

    if (!isAllowedServer(query.url))
        throw new ServerError('Proxying not allowed. The target instance is not in the list of allowed instances.');

    return url.parse(query.url);
}