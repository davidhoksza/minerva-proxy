# Minerva proxy server

A simple node.js proxy server which enables to connect to a Minerva instance from JavaScript.
The proxy server works as follows:

- it accepts the address of a target Minerva instance as a part of the query string 
- if this is the first access to the target server, it retrieves an authentication token and stores it for future reuse
- it creates a request to the target API endpoint using the authentication token and pipes it to the client

The `config.js` file can be modified by creating `local_config.js` with custom settings to:

- set the port on which the proxy will listen
- modify the list of allowed minerva instances
- set whether the proxy server should be http or https
- change the [certificate](https://nodejs.org/api/https.html#https_https_createserver_options_requestlistener) (in case of HTTPS)

## Usage

Install [node.js](https://nodejs.org/en/) and run

```
node server.js
```

To test the server on localhost with default settings try (to access pg-sandbox you need to be in uni.lu domain)

```
http://localhost:8090/?url=https://minerva-dev.lcsb.uni.lu/minerva/api/configuration/
```
or
```
http://localhost:8090/?url=http://pg-sandbox.uni.lu/minerva/api/configuration/
```



 